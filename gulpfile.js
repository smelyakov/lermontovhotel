var gulp = require('gulp'),
    mainBowerFiles = require('main-bower-files'),
    concat = require('gulp-concat'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    browserSync = require('browser-sync'),
    bower = require('bower');

gulp.task('bower', function (cb) {
    bower.commands.install([], {save: true}, {})
        .on('end', function (installed) {
            console.log('Bower ready');
            cb();
        });
});

gulp.task('sass', ['bower'], function () {
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./src/assets/css'))
        .pipe(browserSync.stream({match:'**/*.css'}));
});

gulp.task('sass-prod', ['bower'], function () {
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sass())
        .pipe(prefixer())
        .pipe(minify())
        .pipe(gulp.dest('./src/assets/css'));
});

gulp.task('serve', ['sass'], function () {
    browserSync({
        proxy: 'localhost:8282'
    });

    gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
});

gulp.task('build', ['sass-prod']);

gulp.task('default', ['serve']);
