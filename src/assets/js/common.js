;(function (LH, $) {
  'use strict';

  LH.SpeedBooking = {
    init: function () {
      this.el = $('.speed-booking');
      this.bindUI();
    },
    bindUI: function () {
      var self = this;

      this.el.on('click', '.speed-booking__title', function () {
        self.el.find('.speed-booking__body').toggleClass('speed-booking__body_expanded');
      });
    }
  };

  LH.SpeedBooking.init();
}(window.LH = window.LH || {}, window.jQuery));

(function (LH, $) {
  'use strict';

  LH.Callback = {
    init: function () {
      this.el = $('.callback');
      this.form = $('.callback__form');
      this.bindUI();
    },
    bindUI: function () {
      var self = this;

      this.el
      .on('click', '.callback-btn', function () {
        self.showForm();
      })
      .on('click', '.callback__close', function () {
        self.toggleForm();
      });
    },
    toggleForm: function () {
      this.form.toggleClass('callback__form_visible');
    },
    hideForm: function () {
      this.form.removeClass('callback__form_visible');
    },
    showForm: function () {
      this.form
        .addClass('callback__form_visible')
        .find('input[type="tel"]').focus();
    }
  };

  LH.Callback.init();
}(window.LH = window.LH || {}, window.jQuery));

$(function() {
  if($("a[rel=rokbox]")) {
    $("a[rel=rokbox]").fancybox({
      hideOnContentClick:false,
      hideOnOverlayClick:false,
      overlayShow:false,
      type:'image',
      transitionIn:'',
      transitionOut:'',
      speedIn:'250',
      speedOut:'250'
    });
  }

  $('.header-gallery').slick({
    accessibility: false,
    initialSlide: 2,
    focusOnSelect: true,
    variableWidth: true,
    arrows: true,
    slide: '.header-gallery__item',
    mobileFirst: true,
    centerMode: true,
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          slidesToShow: 3
        }
      }
    ]
  });
});

$(function () {
  $('.nav-toggle, .menu__close').on('click', function () {
    $('#mainmenu').toggleClass('menu_visible');
    $('.page').toggleClass('page_no-scroll');
  });

  $(document).on('submit', '.ajax-form', function (e) {
    e.preventDefault();

	if (!$.trim($('#cb_phone').val())) {return;}

    var target = this,
        $this = $(this);

    if ($this.data('target') !== undefined) {
      target = $this.data('target');
    }

    values = $this.serializeArray();

values.push({name: 'callbackSubmit', value: true});

    $this.find('input[type="submit"]').attr('disabled', 'disabled');

    $.ajax({
      type: 'POST',
      dataType: 'html',
      url: $(this).attr('action'),
      data: values,
      success: function(data) {
        $(target).replaceWith(data);
      }
    });
  });
});