<?php
$chosenServices = !empty($input) ? explode('|', $input) : '';
$lang = $modx->context->getOption('cultureKey', null, 'default');

$output = '';


$services_dict = [
  "single-bed" => ["en" => "Single Bed", "ru" => "Односпальная кровать"],
  "bed" => ["en" => "Double bed", "ru" => "Двуспальная кровать"],
  "shower" => ["en" => "Shower", "ru" => "Душевая кабина"],
  "hair-dryer" => ["en" => "Hairdryer", "ru" => "Фен"],
  "channel-mosaic" => ["en" => "CCTV (60 channels)", "ru" => "Кабельное телевидение (60 каналов)"],
  "wi-fi" => ["en" => "WiFi", "ru" => "Интернет WiFi"],
  "office-phone" => ["en" => "Phone", "ru" => "Телефон (м/г, м/н)"],
  "air-conditioner" => ["en" => "Сonditioner", "ru" => "Кондиционер"],
  "service-bell" => ["en" => "Room-service 24/7", "ru" => "Room-service 24 часа"],
  "safe" => ["en" => "Safe (at reception)", "ru" => "Сейф (на reception-бесплатно)"],
  "towel" => ["en" => "Daily towels change", "ru" => "Смена полотенец - ежедневно"],
  "waiter" => ["en" => "Breakfast", "ru" => "Завтрак"]
];

$output .= '<div class="room-services">';

foreach ($chosenServices as $key => $value) {
  $output .= '<span class="room-services__item">';
  $output .= '<i class="icon room-services__item-icon"><img src="/assets/images/icons/50x50/' . $value . '.png"></i>';
  $output .= '<span class="room-services__item-name">' . $services_dict[$value][$lang] . '</span>';
  $output .= '</span>';
}

$output .= '</div>';

return $output;